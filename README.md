# ml-sats

GitLab repository for the SATS project "Improving the use of Machine Learning at NINA"

# Plan for: **Good practices for Machine Learning at NINA**

**1- Description of SIGMA2 and how to apply for an account**

1.1- What is SIGMA2?
1.2- The role of Sigma2 in the ML workflow
1.3- Getting started with Sigma2

**2- Basic bash commands**

2.1- Copy files over to Sigma2
2.2- Synchronizing your local repository with your remote repository 
2.3- Going futher with bash

**3- Running scripts on Sigma2**
3.1- Keep your folders clean
3.2- Description of job scripts

**4- Using docker on SIGMA2**

4.1- Introduction to docker
4.2- Build your custom docker image
4.3- Introduction to singularity
4.4- Run your script in a docker container

**5- Machine learning workflow**

5.1- Structuring the ML project
5.2- Training and monitoring the training of the model
5.3- Examples of workflow

For each of the small projects describing the ML workflow, make a folder on the git repository

Examples of ML projects that leverage the power of SIGMA2

- Training a model for cats dogs classification (working example with audio)
- Using a pre-trained model for bird song recognition (working example with audio files)
- Do something with satellite data (working example with satellite data)

--- SUPPLEMENTARY WORK ---

- Make a cheatsheet for bash (refer to "section 2.3 - Going further with bash")
- Make a library of pre-trained model that can be useful for us at NINA
